package visibleVariable;


public class Actor {
	// Test access modifier with same variable name but different scope
	private String name = "Julia Robert";
	
	public Actor() {
		String color;
		color = "Tom Cruise";
		System.out.println(this.name);
		System.out.println(color);
	}
	
	public static void main(String[] args) {
		new Actor();
	}
	
}


