package model;

public abstract class Movie {
	private String name;
	
	public Movie() {
		this.name = "Movie";
	}
	
	public Movie(String name) {
		this.name = name;
	}

	public abstract String director();
	
	public String toString() {
		return this.name;
	}
}
