package main;

import java.util.ArrayList;

import visibleVariable.Actor;
import visibleVariable.ForrestGump;
import model.Action;
import model.Movie;
import model.Romance;
import model.Triller;

public class Main {
	public static void main(String[] args) {
		Action action = new Action("The Dark Knight");
		Romance romance = new Romance("50 Shades of Grey");
		Triller triller = new Triller();
		
		System.out.println("\n****Test constructor****");
		System.out.println("title : "+action);
		System.out.println("title : "+romance);

		ArrayList<Movie> movie = new ArrayList<Movie>();
		movie.add(action);
		movie.add(romance);

		for (Movie m: movie) {
			System.out.println("Director : "+m.director());
		}
		
		ForrestGump forrest1 = new ForrestGump();
		ForrestGump forrest2 = new ForrestGump("Rebecca Williams");
		ForrestGump forrest3 = new ForrestGump("Tom Hank"," Comedy");
		
		System.out.println("\n****Implement method****");
		String[] trill = triller.things;
		for (int i=0; i<6; i++) {
			System.out.print(trill[i]);
		}
		
		System.out.println("\n\n****Sample when local variable is shadow****");
		System.out.println(forrest1);
		System.out.println(forrest2);
		System.out.println(forrest3);

	}
}
