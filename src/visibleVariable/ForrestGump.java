package visibleVariable;

public class ForrestGump {
	// local variable is shadow
	private String name;
	private String type;
	
	public ForrestGump() {
		this.name = "Robert Zemeckis";
	}
	
	public ForrestGump(String aName) {
		String name;
		this.name = aName;
	}
	
	public ForrestGump(String bName,String bType) {
		String name;
		String type;
		this.name = bName;
		this.type = bType;
	}
	
	public String toString() {
		return name;
	}

}
